<?php 
require_once('productHandler.php');
class ValidateForm{
    private $data;
    private $errors=[];
    private static $fields = ['SKU','name','price','type'];
    private $han;
    private $properties = ['SKU'=>'','name'=>'','price'=>'']; 
    public function __construct($form_data)
    {
        $this->data = $form_data;
        $this->han= new Handler();
    }
    public function validateForm()
    {
        foreach (self::$fields as $field) {
            if (!array_key_exists($field,$this->data)) {
                trigger_error("$field is not present in data");
                return;
            }
        }
        $this->validateSKU();
        $this->validateName();
        $this->validatePrice();
        $this->validateType();
        return $this->errors;
    }
    public function getProperties()
    {
        return $this->properties;
    }
    private function validateSKU()
    {
        $val = trim($this->data['SKU']);
        $found = $this->checkSKU($val);
        if (empty($val)) {
            $this->addError('SKU','SKU is required');
        }else {
            $this->addProperty('SKU',$val);
            if (!preg_match('/^[a-zA-Z]+[-]+[0-9]+$/',$val)) {
                $this->addError('SKU','SKU should be chars and like this word-number');
            }
            if ($found === true) {
                $this->addError('SKU','SKU should be uniqe');
            }
        }
    }
    private function validateName()
    {
        $val = $this->data['name'];
        if (empty($val)) {
            $this->addError('name','Name is required');
        }else {
            $this->addProperty('name',$val);
            if (!preg_match('/^[a-zA-Z\s]*$/',$val)) {
                $this->addError('name','Name must be letters only');
            }
        }
    }
    private function validatePrice()
    {
        $val = trim($this->data['price']);
        if (empty($val)) {
            $this->addError('price','Price is required');
        }else {
            $this->addProperty('price',$val);
            if (!preg_match('/^[0-9]+$/',$val)) {
                $this->addError('price','price must be integer');
            }
        }
    }
    private function validateType()
    {
        $val = $this->data['type'];
        if ($val =='Select Product Type') {
            $this->addError('type','Type is required');
        }      
    }
    private function addError($key,$error)
    {
        $this->errors[$key] = $error;
    }
    private function checkSKU($val){
       $SKUs = $this->han->getAllSKU();
       $found = false;
        foreach ($SKUs as $SKU) { 
            if ($SKU['SKU'] === $val ) {
                $found = true;
                break;
            }else {
                $found = false;
            }
        }
        return $found;
    }
    private function addProperty($key,$property)
    {
        $this->properties[$key] = $property;
    }
}


?>