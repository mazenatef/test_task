<?php 
include_once('config/db_connection.php');
abstract class Product
    {
        protected $SKU;
        protected $name;
        protected $price;
        protected $type;

        public function __construct(){
            
        }
        //setters
        public function setSKU($sku){
            $this->SKU=$sku;
        }
        public function setName($name){
            $this->name=$name;
        }
        public function setPrice($price){
            $this->price=$price;
        }
        public function setType($type){
            $this->type=$type;
        }
        //getters
        public function getSKU(){
            return $this->SKU;
        }
        public function getName(){
           return $this->name;
        }
        public function getPrice(){
            return $this->price;
        }
        public function getType(){
            return $this->type;
        }
        abstract function addProduct($product);
              
    }
    
   
?>